import logging
import math
import azure.functions as func

def integrate(lower, upper):
    ns = [10**x for x in range(1, 8)]
    result = []
    for n in ns:
        integral = 0
        dx = (float(upper) - float(lower)) / n
        for i in range(n):
            val = dx * (i + 0.5)
            dI = abs(math.sin(val)) * dx
            integral += dI

        result.append(float(integral))

    return ", ".join([str(el) for el in result])

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = req.route_params.get('lower')
    upper = req.route_params.get('upper')

    if lower is not None and upper is not None:
        result = integrate(lower, upper)
        return func.HttpResponse(result)
    else:
        return func.HttpResponse(
             "Welcome to my web app. Go to the route /api/lower/upper (i.e /api/0/3.14159) to do computations."
        )
