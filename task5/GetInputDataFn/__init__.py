# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from azure.storage.blob import BlobServiceClient


def main(container: str) -> list:
    blob_service_client = BlobServiceClient.from_connection_string("DefaultEndpointsProtocol=https;AccountName=cloudshw2task5;AccountKey=Pa1AJgmkJ8Q9BcU4OHwGq3rIURHkCSbusryYF6OSpm8QybLUpJElJLbdCsVv1dgjl6k0jsSJXTvL+AStu+NEDg==;EndpointSuffix=core.windows.net")
    blob_client = blob_service_client.get_container_client(container=container)
    
    blob_list = blob_client.list_blobs()
    result = []
    i = 0
    for blob in blob_list:
        lines = blob_client.download_blob(blob).readall().decode("utf-8").splitlines()
        for line in lines:
            result.append((i, line))
        i += 1

    return result