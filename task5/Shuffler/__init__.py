# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(pairs: list) -> list:
    result_dict = {}
    for pair in pairs:
        k = pair[0]
        v = pair[1]
        if k not in result_dict:
            result_dict[k] = [v]
        else:
            result_dict[k].append(v)
    return list(result_dict.items())
