# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    lyrics = yield context.call_activity('GetInputDataFn', "filecontainer")
    mappers = [context.call_activity('Mapper', line) for line in lyrics]
    mappers_result = yield context.task_all(mappers)
    shuffler = yield context.call_activity('Shuffler', [map for mapper in mappers_result for map in mapper])
    reducers = [context.call_activity('Reducer', pair) for pair in shuffler]
    reducers_result = yield context.task_all(reducers)

    return reducers_result

main = df.Orchestrator.create(orchestrator_function)