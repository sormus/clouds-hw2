import numpy as np
from flask import Flask

app = Flask(__name__)

@app.route('/<lower>/<upper>')
def integrate(lower, upper):
    ns = [10**x for x in range(1, 8)]
    result = []
    for n in ns:
        integral = 0
        dx = (float(upper) - float(lower)) / n
        for i in range(n):
            val = dx * (i + 0.5)
            dI = np.abs(np.sin(val)) * dx
            integral += dI
            
        result.append(integral)
        
    return result

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
