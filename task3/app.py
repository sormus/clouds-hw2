import numpy as np
from flask import Flask

app = Flask(__name__)

@app.route('/')
def homepage():
    return "Welcome to my web app. Go to the route /lower/upper (i.e /0/3.14159) to do computations."

@app.route('/<lower>/<upper>')
def integrate(lower, upper):
    ns = [10**x for x in range(1, 8)]
    result = []
    for n in ns:
        integral = 0
        dx = (float(upper) - float(lower)) / n
        for i in range(n):
            val = dx * (i + 0.5)
            dI = np.abs(np.sin(val)) * dx
            integral += dI

        result.append(float(integral))

    return ", ".join([str(el) for el in result])

if __name__ == '__main__':
    app.run()
